# translation of mumble debconf to Portuguese
# Copyright (C) 2008 the mumble's copyright holder
# This file is distributed under the same license as the mumble package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: mumble 1.2.1-3\n"
"Report-Msgid-Bugs-To: mumble@packages.debian.org\n"
"POT-Creation-Date: 2010-01-11 16:52+0100\n"
"PO-Revision-Date: 2010-01-17 12:03+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid "Password to set on SuperUser account:"
msgstr "Palavra-passe a definir na conta SuperUser:"

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid ""
"Murmur has a special account called \"SuperUser\" which bypasses all "
"privilege checks."
msgstr ""
"O murmur possui uma conta especial chamada \"SuperUser\" a qual ultrapassa "
"todas as verificações de privilégios."

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid ""
"If you set a password here, the password for the \"SuperUser\" account will "
"be updated."
msgstr ""
"Se você definir aqui uma palavra-passe, a palavra-passe para a conta "
"\"SuperUser\" será actualizada."

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid "If you leave this blank, the password will not be changed."
msgstr "Se você deixar isto vazio, a palavra-passe não será alterada."

#. Type: boolean
#. Description
#: ../mumble-server.templates:2001
msgid "Autostart mumble-server on server boot?"
msgstr "Arrancar automaticamente o mumble-server no arranque do servidor?"

#. Type: boolean
#. Description
#: ../mumble-server.templates:2001
msgid ""
"Mumble-server (mumble-server) can start automatically when the server is booted."
msgstr ""
"O mumble-server (mumble-server) pode ser arrancado automaticamente durante o "
"arranque do servidor."

#. Type: boolean
#. Description
#: ../mumble-server.templates:3001
msgid "Allow mumble-server to use higher priority?"
msgstr "Permitir ao mumble-server usar prioridade mais alta?"

#. Type: boolean
#. Description
#: ../mumble-server.templates:3001
msgid ""
"Mumble-server (mumble-server) can use higher process and network priority to "
"ensure low latency audio forwarding even on highly loaded servers."
msgstr ""
"O mumble-server (mumble-server) pode usar prioridade de processo e rede mais alta "
"para assegurar o envio de áudio de baixa latência mesmo em servidores muito "
"carregados."

#~ msgid "Email address to send registration emails from:"
#~ msgstr "Endereço de email para utilizar para o envio de emails de registo:"

#~ msgid ""
#~ "Murmur comes with a web-based registration script, which will send an "
#~ "authentication code to the user by email before registration can be "
#~ "completed."
#~ msgstr ""
#~ "O murmur vem com um script de registos baseado em web, o qual irá enviar "
#~ "um código de autenticação para o utilizador por email, antes que cada "
#~ "registo possa ser completado."

#~ msgid ""
#~ "Set this to the email address you wish such authentication emails to come "
#~ "from. If you set it blank, registration will be disabled."
#~ msgstr ""
#~ "Configure isto para o endereço de email que deseja que os tais emails de "
#~ "autenticação venham de (From:). Se você deixar em branco, os registos "
#~ "serão desactivados."
