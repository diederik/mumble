Welcome to Mumble, a low-latency encrypted audio and text chat client which
is free open source software, first released to Debian in 2008.

Starting with Mumble 1.5.517 release candidate, the client only supports the
OPUS codec and all other codec support has been removed. Historically Mumble
supported CELT 0.7.1 as the base communication codec, so versions Mumble older
than mid-2012 that do not have OPUS support will have text messaging work but
will not be able to communicate via audio.

The best way to test the Mumble client to insure it's set up and working after
going through the Wizard to test the basic microphone settings is to configure
the Audio Output settings under the "Loopback Test" at the bottom to "Server",
Apply the changes, and then connect to a remote server and listen to the audio
coming back from the server from the local microphone. This will allow
"hearing what others will hear from your mic" so that the mic audio level can
be adjusted as desired. (Remember to turn the Loopback Test back to "None".)

Other current items to be aware of:

Autoscroll of the chat window is not working properly on Linux.
  https://github.com/mumble-voip/mumble/issues/2504
  https://github.com/mumble-voip/mumble/issues/4638

The upstream summary of changes for Mumble 1.5.517-rc can be found here:
  https://www.mumble.info/blog/mumble-1.5.517-rc/

Some notes about Mumble and Tor (Tor = "The Onion Router"):
------------------------------------------------------------------------------
If you have never heard of Tor before, it is not just "the dark web"; it
is also a very useful tool you can use at home and at work.

Mumble and mumble-server can be used in either UDP or TCP mode. Tor is only
capable of TCP connections. In TCP mode Mumble is capable of being run over
Tor to hide the originating IP address of the client. Mumble audio over Tor
can work well. Mmumble-server can be used over a Tor Onion Service to run
a server from an IP address behind a NAT (e.g. from your server at home) and
still allow remote connection from the outside world. This works because a
Tor Onion Service creates a .onion address name that exists at a
"rendezvous point" at a Tor Exit Node in the public Internet space which is
then connected to from Mumble running over Tor. So there's good reasons to
want to run Mumble and Mumble-server over Tor even if you may not need the
extra privacy and security Tor can provide.

Instructions for creating a Tor Onion Service with mumble-server is in the
README.Debian file for the mumble-server package.

There are two ways to operate Mumble through Tor; by starting Mumble via
Torsocks, and setting Mumble to connect to a local Tor daemon via a Proxy.

To start Mumble through Tor, install the 'torsocks' package, and
run 'torsocks mumble' to start Mumble through Tor, and configure Mumble
under Network to "Force TCP mode" to shorten the time Mumble would take
to sense that UDP connections were blocked. If starting Mumble this way is
desired regularly, it should be possible to create a custom menu icon in your
desired window manager or desktop environment to execute the needed command
so that starting 'torsocks mumble' via a terminal won't be nececessary.

To use Mumble client through Tor as a Proxy, install the 'tor' package,
then in the Network settings under Proxy select Proxy type "SOCKS5 proxy",
Hostname "127.0.0.1" and Port "9050". As soon as the "SOCKS5" proxy type is
selected the "Force TCP mode" option will grey out because using a Proxy
automatically requires TCP mode. This method can also be useful for networks
that have a dedicated nonlocal Tor server to make connections through.
------------------------------------------------------------------------------
